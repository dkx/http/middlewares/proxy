import {Middleware, NextMiddlewareFunction, Request, RequestState, Response} from '@dkx/http-server';
import fetch, {RequestInit, Headers, Response as FetchResponse, FetchError} from 'node-fetch';
import {performance} from 'perf_hooks';
import {parse as urlParse} from 'url';
import {ProxyOptions} from './config';


export function proxyMiddleware(options: ProxyOptions): Middleware
{
	return async (req: Request, res: Response, next: NextMiddlewareFunction, state: RequestState): Promise<Response> =>
	{
		const requestUrl = urlParse(req.url);
		const headers = new Headers;

		for (let headerName in req.headers) {
			if (passHeader(headerName, options)) {
				headers.set(headerName, <string>req.headers[headerName]);
			}
		}

		headers.set('host', hostToHostHeader(options.host));

		const init: RequestInit = {
			method: req.method,
			timeout: options.timeout,
			headers: headers,
		};

		if (['GET', 'HEAD'].indexOf(req.method) < 0 && typeof req.body !== 'undefined') {
			init.body = req.body;
		}

		let response: FetchResponse;
		const start = performance.now();

		try {
			response = await fetch(options.host + requestUrl.path, init);
		} catch (e) {
			if (e instanceof FetchError && e['type'] === 'request-timeout') {
				return sendRequestTimeout(res);
			}

			return sendRequestError(res);
		}

		const responseBody = await response.buffer();
		const end = performance.now();

		state.proxy = {
			responseTime: end - start,
			host: options.host,
		};

		res = res.withStatus(response.status, response.statusText);

		response.headers.forEach((headerValue, headerName) => {
			res = res.withHeader(headerName, headerValue);
		});

		res.write(responseBody);

		return next(res);
	}
}


function passHeader(name: string, options: ProxyOptions): boolean
{
	if (options.passAllHeaders === true) {
		return true;
	}

	if (typeof options.passHeaders !== 'undefined' && options.passHeaders.indexOf(name) >= 0) {
		return true;
	}

	return false;
}


function hostToHostHeader(host: string): string
{
	const url = urlParse(host);
	const parts = [
		url.hostname,
	];

	if (url.port !== '80') {
		parts.push(url.port);
	}

	return parts.join(':');
}


function sendRequestTimeout(res: Response): Response
{
	res = res.withStatus(504);
	res.write('Endpoint request timed out');

	return res;
}


function sendRequestError(res: Response): Response
{
	res = res.withStatus(500);
	res.write('Server error');

	return res;
}
