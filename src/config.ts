export declare interface ProxyOptions
{
	host: string,
	timeout?: number,
	passAllHeaders?: boolean,
	passHeaders?: Array<string>,
}
