# DKX/Http/Middleware/Proxy

Proxy middleware for [@dkx/http-server](https://gitlab.com/dkx/http/server).

## Installation

```bash
$ npm install --save @dkx/http-middleware-proxy
```

or with yarn

```bash
$ yarn add @dkx/http-middleware-proxy
```

## Usage

```js
const {Server} = require('@dkx/http-server');
const {proxyMiddleware} = require('@dkx/http-middleware-proxy');

const app = new Server;
const proxyOptions = {
    host: 'http://example.com',
    timeout: 5000,
    passHeaders: ['X-Custom-header'],
    passAllHeaders: true
};

app.use(proxyMiddleware(proxyOptions));
```

Only the `host` option is required.
