import {Request, testMiddleware} from '@dkx/http-server';
import {ResponseServer} from '@dkx/http-response-server';
import {expect} from 'chai';
import {performance} from 'perf_hooks';
import {proxyMiddleware} from '../../lib';


let server: ResponseServer;


describe('#proxyMiddleware', () => {

	beforeEach((done) => {
		server = new ResponseServer;
		server.run(8000, done);
	});

	afterEach((done) => {
		server.close(done);
	});

	it('should proxy simple GET request', async () => {
		const body: Array<string> = [];

		server.respondWith({
			body: 'hello world',
		});

		await testMiddleware(proxyMiddleware({
			host: 'http://localhost:8000',
		}), {
			url: 'http://proxy.localhost/test?a=A&b=B',
			onBodyWrite: (chunk) => body.push(chunk.toString()),
		});

		expect(body).to.be.eql(['hello world']);
	});

	it('should set host header without port', async () => {
		server.respondWith({
			onRequest: (req: Request) => {
				expect(req.getHeader('host')).to.be.equal('localhost');
			},
		});

		await testMiddleware(proxyMiddleware({
			host: 'http://localhost:80',
		}), {
			url: 'http://proxy.localhost',
		});
	});

	it('should set host header with port', async () => {
		server.respondWith({
			onRequest: (req: Request) => {
				expect(req.getHeader('host')).to.be.equal('localhost:8000');
			},
		});

		await testMiddleware(proxyMiddleware({
			host: 'http://localhost:8000',
		}), {
			url: 'http://proxy.localhost',
		});
	});

	it('should pass all request headers to target', async () => {
		server.respondWith({
			onRequest: (req: Request) => {
				expect(req.getHeader('X-Test')).to.be.equal('lorem ipsum');
			},
		});

		await testMiddleware(proxyMiddleware({
			host: 'http://localhost:8000',
			passAllHeaders: true,
		}), {
			url: 'http://proxy.localhost',
			headers: {
				'X-Test': 'lorem ipsum',
			},
		});
	});

	it('should pass specific headers to target', async () => {
		server.respondWith({
			onRequest: (req: Request) => {
				expect(req.getHeader('X-Allowed')).to.be.equal('true');
			},
		});

		await testMiddleware(proxyMiddleware({
			host: 'http://localhost:8000',
			passHeaders: ['X-Allowed'],
		}), {
			url: 'http://proxy.localhost',
			headers: {
				'X-Allowed': 'true',
				'X-Denied': 'true',
			},
		});
	});

	it('should set response status from target', async () => {
		server.respondWith({
			statusCode: 404,
			statusMessage: 'Not found',
		});

		const res = await testMiddleware(proxyMiddleware({
			host: 'http://localhost:8000',
		}), {
			url: 'http://proxy.localhost',
		});

		expect(res.statusCode).to.be.equal(404);
		expect(res.statusMessage).to.be.equal('Not found');
	});

	it('should set response headers from target', async () => {
		server.respondWith({
			headers: {
				'x-test': 'lorem ipsum',
			},
		});

		const res = await testMiddleware(proxyMiddleware({
			host: 'http://localhost:8000',
		}), {
			url: 'http://proxy.localhost',
		});

		expect(res.hasHeader('x-test')).to.be.equal(true);
		expect(res.getHeader('x-test')).to.be.equal('lorem ipsum');
	});

	it('should fail with timeout', async () => {
		server.respondWith({
			timeout: 500,
		});

		const body: Array<string> = [];
		const start = performance.now();

		const res = await testMiddleware(proxyMiddleware({
			host: 'http://localhost:8000',
			timeout: 100,
		}), {
			url: 'http://proxy.localhost',
			onBodyWrite: (chunk) => body.push(chunk.toString()),
		});

		const end = performance.now();
		const elapsed = Math.round((end - start) * 1000) / 1000;

		expect(res.statusCode).to.be.equal(504);
		expect(body).to.be.eql(['Endpoint request timed out']);
		expect(elapsed).to.be.greaterThan(100).and.below(500);
	});

});
